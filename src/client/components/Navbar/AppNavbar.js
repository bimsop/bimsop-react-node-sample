
import { LinkContainer } from 'react-router-bootstrap'
import React, { PropTypes } from 'react'
import AboutDlg from 'Dialogs/AboutDlg'
import ServiceManager from 'SvcManager'
import './AppNavbar.scss'

import {
  DropdownButton,
  NavDropdown,
  MenuItem,
  NavItem,
  Navbar,
  Button,
  Modal,
  Nav
  } from 'react-bootstrap'

export default class AppNavbar extends React.Component {

  /////////////////////////////////////////////////////////
  //
  //
  /////////////////////////////////////////////////////////
  constructor (props) {

    super(props)

    this.state = {
      aboutOpen:    false
    }

  }

  /////////////////////////////////////////////////////////
  //
  //
  /////////////////////////////////////////////////////////
  openAboutDlg () {

    this.setState(Object.assign({}, this.state, {
      aboutOpen: true
    }))
  }


  /////////////////////////////////////////////////////////
  //
  //
  /////////////////////////////////////////////////////////
  render() {

    const { appState } = this.props

    const {user} = appState

    const username = user
      ? `${user.firstName} ${user.lastName}`
      : ''

    return (

      <Navbar className="forge-navbar">
        <Navbar.Header>
          <Navbar.Brand>
            <NavItem className="forge-brand-item"
              href="#"
              target="_blank">
             
            </NavItem>
          </Navbar.Brand>
          <Navbar.Toggle/>
        </Navbar.Header>

        <Navbar.Collapse>

          {
            appState.navbar.links.home &&

            <Nav>
              <LinkContainer to={{ pathname: '/', query: { } }}>
                <NavItem eventKey="home">
                  <label className="nav-label">
                    &nbsp; Home
                  </label>
                </NavItem>
              </LinkContainer>
            </Nav>
          }

          <Nav pullRight>


            {
              appState.navbar.links.about &&

              <NavItem eventKey="about" onClick={() => {this.openAboutDlg()}}>
                <label className="nav-label">
                  &nbsp; About ...
                </label>
              </NavItem>
            }
          </Nav>

        </Navbar.Collapse>

        <AboutDlg
          close={()=>{ this.setState(Object.assign({}, this.state, {
            aboutOpen: false
          }))}}
          open={this.state.aboutOpen}
        />

      </Navbar>
    )
  }
}
