const ExtensionId = 'Viewing.Extension.Demo'

export default class DemoExtension
  extends Bimsop.Viewing.Extension {

  constructor (viewer, options) {

    super()

    this.viewer = viewer

    this.onSelectionChanged = this.onSelectionChanged.bind(this);

  }

  load () {

    this.viewer.setBackgroundColor(
      255, 226, 110,
      219, 219, 219)



    this.viewer.addEventListener(
      Bimsop.Viewing.SELECTION_CHANGED_EVENT,
      this.onSelectionChanged
    );

    console.log (`${ExtensionId} loaded`)

    return true
  }

   /////////////////////////////////////////////////////////////////
  // selection changed callback
  //
  /////////////////////////////////////////////////////////////////
  onSelectionChanged (event) {

    console.log(this.viewer);

  if (event.dbIdArray.length) {
      var dbId = event.dbIdArray[0];

      console.log(`selected node dbId : ` + dbId);


      this.viewer.fitToView(dbId);

      this.viewer.isolate(dbId);

    } else {
  

      this.viewer.isolate([]);

      this.viewer.fitToView();


    }
  }

  unload () {

    console.log (`${ExtensionId} unloaded`)

    return true
  }
}

Bimsop.Viewing.theExtensionManager.registerExtension(
  ExtensionId, DemoExtension)

