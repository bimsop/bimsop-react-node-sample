import Viewer from 'Viewer'
import './ViewerView.scss'
import React from 'react'

class ViewerView extends React.Component {

   /////////////////////////////////////////////////////////
   //
   //
   /////////////////////////////////////////////////////////
   constructor () {

      super ()
   }

   /////////////////////////////////////////////////////////
   // Initialize viewer environment
   //
   /////////////////////////////////////////////////////////
   initialize (options) {

      return new Promise((resolve, reject) => {

        Bimsop.Viewing.Initializer (options, () => {

          resolve ()

        }, (error) => {

          reject (error)
        })
      })
   }




   /////////////////////////////////////////////////////////
   // viewer div and component created
   //
   /////////////////////////////////////////////////////////
   async onViewerCreated (viewer) {

     let { id, extIds, path } =
       this.props.location.query

     console.log(this.props.location.query);


    // 轻量转换后的模型路径，这个示例采用云端的一个示例模型，如果需要自己的模型，您可以和我们联系购买轻量化引擎或服务。
      var docs = [
        { path: ['http://gb9.bimsop.com/peacock/%E5%88%AB%E5%A2%85%E9%A1%B9%E7%9B%AE2016-1497078013339/%E5%88%AB%E5%A2%85%E9%A1%B9%E7%9B%AE2016-1497078013339.gbim'], name: '小别墅' },
      ];


     var options = {
       env: 'Local',
       docid: docs[0].path
     };

     await this.initialize(options);

  
      viewer.start();
      viewer.addEventListener(Bimsop.Viewing.MODEL_STRUCTURE_TREE_MERGED_EVENT, function (tree) { console.log(tree); });
      viewer.addEventListener(Bimsop.Viewing.MODEL_LEVEL_TREE_MERGED_EVENT, function (e) { console.log(e.tree); });
      viewer.addEventListener(Bimsop.Viewing.ALL_DONE_EVENT, function (e) {
        console.log('all model loaded');


        if (extIds) {

          const extensionIds = extIds.split(';')

          for (let extensionId of extensionIds) {

            console.log(extensionId);
            viewer.loadDynamicExtension(extensionId)
          }
        }


      });

      viewer.addEventListener(Bimsop.Viewing.SELECTION_CHANGED_EVENT, function (e) {

        console.log(e);
      });

     
    


      viewer.load(options.docid, 
        { modelName: 'test model name' }, 
        function (model) { }, 
        function (err) { 
          if (err) console.log(err) 
        });



 
   }

   /////////////////////////////////////////////////////////
   //
   //
   /////////////////////////////////////////////////////////
   render () {

      return (
        <div className="viewer-view">
          <Viewer onViewerCreated={(viewer => {
              this.onViewerCreated(viewer)
            })}
          />
        </div>
      )
   }
}

export default ViewerView


