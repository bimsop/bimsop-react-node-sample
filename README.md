
# 使用React技术和轻量BIM开发BIM+应用


基于React + Node.js 开发BIM+ 轻量化应用的示例。

### Thumbnail


 * 前端

  [React](https://facebook.github.io/react/) + [Redux](https://github.com/reactjs/redux)

 * 后端:

  [Node.js](https://nodejs.org) + [Express](http://expressjs.com)

 * 构建系统:

  [NPM](https://www.npmjs.com/) scripts + [Webpack 2](https://webpack.js.org) + [Babel](https://babeljs.io)

## React Support

React >= 0.13.x

## Browser Support

浏览器支持:

  * Chrome
  * Firefox
  * Safari
  * Opera
  * Edge


## 如何运行示例


 * `npm install`    *(下载相关module)*


 * `set HOT_RELOADING=true`

 * `npm run build`

 * `npm run build-server`

 * `npm start`      

 * 浏览器中打开 [http://localhost:3000](http://localhost:3000) 


## 关键代码解释

* 创建viewer对象， 请参见`/src/client/components/Viewer.Components/Viewer/Viewer.js` 中 `componentDidMount` 部分

* viewer初始化，并加载轻量化模型，加载extension，请参见`/src/client/routes/Viewer/components/ViewerView.js`部分。请注意到`viewer.loadDynamicExtension(extensionId)`
	
* 推荐以viewer extension的方式开发，基于ES6的extension的示例请参考`/src/client/components/Viewer.Components/Extensions/Dynamic/Viewing.Extension.Demo/Viewing.Extension.Demo.js`




本示例参考[forge react starter]( https://github.com/Autodesk-Forge/forge-react-boiler.nodejs) 和 [React Redux Starter Kit](https://github.com/davezuko/react-redux-starter-kit).


