
/////////////////////////////////////////////////////////////////////
// DEVELOPMENT configuration
//
/////////////////////////////////////////////////////////////////////
const HOST_URL = process.env.HOST_URL || 'http://localhost'
const PORT = process.env.PORT || 443

const config = {

  env: 'production',

  client: {
    // this the public host name of your server for the
    // client socket to connect.
    // eg. https://myforgeapp.mydomain.com
    host: `${HOST_URL}`,
    env: 'production',
    port: PORT
  },

  forge: {

    viewer: {
      viewer3D: 'http://gb9.bimsop.com/bimsopviewer/release/v0.3/viewer.min.js',
      threeJS: 'http://gb9.bimsop.com/bimsopviewer/release/v0.3/three.min.js',
      style: 'http://gb9.bimsop.com/bimsopviewer/release/v0.3/style-bottom.css'
    }
  }
}

module.exports = config




