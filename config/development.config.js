
/////////////////////////////////////////////////////////////////////
// DEVELOPMENT configuration
//
/////////////////////////////////////////////////////////////////////
const HOST_URL = process.env.HOST_URL ||  'http://localhost'
const PORT = process.env.PORT || 3000

const config = {

  env: 'development',

  client: {
    host: `${HOST_URL}`,
    env: 'development',
    port: PORT
  },

  bimsop: {

    viewer: {
      viewer3D: 'http://gb9.bimsop.com/bimsopviewer/release/v0.3/viewer.min.js',
      threeJS:  'http://gb9.bimsop.com/bimsopviewer/release/v0.3/three.min.js',
      style:    'http://gb9.bimsop.com/bimsopviewer/release/v0.3/style-bottom.css'
    }
  }
}

module.exports = config


